﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Castitate.GUI.Model
{
	[Serializable]
	public class ImageTextHolder
	{
		/// <summary>
		/// Хранит в себе класс Image
		/// </summary>
		public object ImageSource { get; set; }
		public string Text { get; set; }
		public string Tag { get; set; }		
		public long ID { get; set; }
	}
}
