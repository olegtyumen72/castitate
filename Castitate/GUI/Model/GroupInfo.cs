﻿using Castitate.GUI.ViewModel;
using Castitate.Logic.DataClasses;
using Castitate.Logic.Interfaces;
using System;

namespace Castitate.GUI.Model
{
    public class Groups : ViewModelBase
    {
        string _title = string.Empty;
        bool _isChecked = false;
        int _membersCount = 0;
        DateTime _dateOfLastPost;
		public VDBState State { get; set; }
		public long ID { get; set; }

		string _imagePath;

		public string ImagePath
		{
			get
			{
				return _imagePath;
			}
			set
			{
				_imagePath = value;
				OnPropertyChanged("ImagePath");
			}
		}

		public Groups(VkGroupData group)
        {
			Title = group.Name;
			MembersCount = group.Details["MembersCount"] == null ? 0 : (int)group.Details["MembersCount"];
			DateOfLastPost = group.Date;
			State = group.State;
			ID = group.ID;
			ImagePath = group.ImagePath;
		}

		public Groups(VkDataBase group)
		{
			Title = group.Name;
			MembersCount = group.Details["MembersCount"] == null ? 0 : (int)group.Details["MembersCount"];
			DateOfLastPost = group.Date;
			State = group.State;
			ID = group.ID;
			ImagePath = group.ImagePath;
		}

		public Groups() { }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                OnPropertyChanged("Title");
            }
        }

        public bool IsChecked
        {
            get
            {
                return _isChecked;
            }
            set
            {
                _isChecked = value;
                OnPropertyChanged("IsChecked");
            }
        }

        public int MembersCount
        {
            get
            {
                return _membersCount;
            }
            set
            {
                _membersCount = value;
                OnPropertyChanged("MembersCount");
            }
        }

        public DateTime DateOfLastPost
        {
            get
            {
                return _dateOfLastPost;
            }
            set
            {
                _dateOfLastPost = value;
                OnPropertyChanged("DateOfLastPost");
            }
        }
    }
}
