﻿using Castitate.GUI.ViewModel;
using Castitate.Logic.Interfaces;
using System;
using System.Windows.Controls;

namespace Castitate.GUI.Model
{
    public class EventInfo : ViewModelBase
    {
        string _title = string.Empty;
        bool _isChecked = false;
        bool _isPassed;
        int _membersCount = 0;
        DateTime _eventDate;
        string _imagePath;
		

        public EventInfo() { }

		public EventInfo(VkDataBase ev)
		{
			ID = ev.ID;
			EventDate = ev.Date;
			ImagePath = ev.ImagePath;
			IsChecked = false;
			Title = ev.Name;
			MembersCount = Convert.ToInt32(ev.Details["MembersCount"]);
			State = ev.State;
		}

		public VDBState State { get; set; }

		public long ID { get; set; }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                OnPropertyChanged("Title");
            }
        }

        public string ImagePath
        {
            get
            {
                return _imagePath;
            }
            set
            {
                _imagePath = value;
                OnPropertyChanged("ImagePath");
            }
        }

        public bool IsChecked
        {
            get
            {
                return _isChecked;
            }
            set
            {
                _isChecked = value;
                OnPropertyChanged("IsChecked");
            }
        }

        public bool IsPassed
        {
            get
            {
                return _isPassed;
            }
            set
            {
                _isPassed = value;
                OnPropertyChanged("IsPassed");
            }
        }

        public int MembersCount
        {
            get
            {
                return _membersCount;
            }
            set
            {
                _membersCount = value;
                OnPropertyChanged("MembersCount");
            }
        }

        public DateTime EventDate
        {
            get
            {
                return _eventDate;
            }
            set
            {
                _eventDate = value;
                OnPropertyChanged("EventDate");
            }
        }
    }
}
