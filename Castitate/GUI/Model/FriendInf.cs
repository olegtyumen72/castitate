﻿using Castitate.GUI.ViewModel;
using Castitate.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Castitate.GUI.Model
{

	class FriendInf: ViewModelBase
	{
		string _title = string.Empty;
		bool _isChecked = false;
		long _membersCount = 0;
		DateTime _dateOfLastPost;		
		string _imagePath;

		public VDBState State { get; set; }
		public long ID { get; set; }
		public string ImagePath
		{
			get
			{
				return _imagePath;
			}
			set
			{
				_imagePath = value;
				OnPropertyChanged("ImagePath");
			}
		}
		public string Title
		{
			get
			{
				return _title;
			}
			set
			{
				_title = value;
				OnPropertyChanged("Title");
			}
		}
		public bool IsChecked
		{
			get
			{
				return _isChecked;
			}
			set
			{
				_isChecked = value;
				OnPropertyChanged("IsChecked");
			}
		}
		public long MembersCount
		{
			get
			{
				return _membersCount;
			}
			set
			{
				_membersCount = value;
				OnPropertyChanged("MembersCount");
			}
		}
		public DateTime DateOfLastPost
		{
			get
			{
				return _dateOfLastPost;
			}
			set
			{
				_dateOfLastPost = value;
				OnPropertyChanged("DateOfLastPost");
			}
		}

		public FriendInf(VkDataBase friend)
		{
			Title = friend.Name;
			MembersCount = friend.Details["MembersCount"] == null ? 0 : (long)friend.Details["MembersCount"];
			DateOfLastPost = friend.Date;
			State = friend.State;
			ID = friend.ID;
			ImagePath = friend.ImagePath;
		}

	}
}
