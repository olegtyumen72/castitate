﻿using Castitate.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Castitate.GUI.Model
{
	[Serializable]
	public class SettingsData
	{

		List<ImageTextHolder> _whiteList = new List<ImageTextHolder>();


		public List<ImageTextHolder> WhiteList
		{
			get
			{
				return _whiteList;
			}
			set
			{
				_whiteList = value;				
			}
		}

		int _dtd = 5;

		//public List<string> HashtagList { get; set; }
		public int DaysToDie {
			get
			{
				return _dtd;
			}
			set
			{
				_dtd = value;
				VkManager.Instance.CastSettings.OnPropertyChanged("DaysToDie");
			}
		}
		public bool IsSaveMainData { get; set; } = false;

	}
}
