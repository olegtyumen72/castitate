﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Castitate.GUI
{
	/// <summary>
	/// Interaction logic for LogInUC.xaml
	/// </summary>
	public partial class LogInUC : UserControl
	{
		static DependencyProperty LoginDP = DependencyProperty.Register("Login",
			typeof(string), typeof(LogInUC), new PropertyMetadata(""));
		static DependencyProperty PassDP = DependencyProperty.Register("Pass",
			   typeof(string), typeof(LogInUC), new PropertyMetadata(""));
		static RoutedEvent LoginClickDP = EventManager.RegisterRoutedEvent("LoginClick", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(LogInUC));
		static DependencyProperty MistakeDP = DependencyProperty.Register("MistakeText",
			typeof(string), typeof(LogInUC), new PropertyMetadata(""));

		public string MistakeText
		{
			get { return (string)GetValue(MistakeDP); }
			set
			{
				SetValue(MistakeDP, value);
				if (value.Length > 0)
				{
					MistText.Visibility = Visibility.Visible;
				}
				else
				{
					MistText.Visibility = Visibility.Hidden;
				}
			}
		}

		public bool IsRememberMe
		{
			get;
			set;
		}

		public event RoutedEventHandler LoginClick
		{
			add { AddHandler(LoginClickDP, value); }
			remove { RemoveHandler(LoginClickDP, value); }
		}
		public string Login
		{
			get { return GetValue(LoginDP) as string; }
			set { SetValue(LoginDP, value); }
		}

		public string Password
		{
			get { return GetValue(PassDP) as string; }
			set { SetValue(PassDP, value); }
		}

		public LogInUC()
		{
			InitializeComponent();
			Loaded += (v, c) =>
			{
				Ps.Password = Password;
			};
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			var e1 = new RoutedEventArgs(LoginClickDP, this);
			RaiseEvent(e1);
		}

		private void TextBox_GotFocus(object sender, RoutedEventArgs e)
		{
			MistText.Visibility = Visibility.Collapsed;
		}

		private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
		{
			Password = (sender as PasswordBox).Password;
		}
	}
}
