﻿using Castitate.GUI.Model;
using Castitate.GUI.ViewModel;
using System.Windows.Controls;

namespace Castitate.GUI.Views
{
    public partial class GroupsView : UserControl
    {
        public GroupsView()
        {
            InitializeComponent();

            DataContext = new GroupsViewModel();
			var vv = DataContext as GroupsViewModel;

			vv.AttachFilter(
				(str) =>
				{
					try
					{
						var el = str as Groups;
						var text = (CombFilter.SelectedItem as TextBlock).Text;
						switch (text)
						{
							case "None":
								{
									return true;
								}
							case "Soon":
								{
									if (el.State == Logic.Interfaces.VDBState.Soon) return true;
									break;
								}
							case "Passed":
								{
									if (el.State == Logic.Interfaces.VDBState.Passed) return true;
									break;
								}
							case "Future":
								{
									if (el.State == Logic.Interfaces.VDBState.Future) return true;
									break;
								}
						}
						return false;
					}
					catch
					{
						return false;
					}
				});

			CombFilter.SelectionChanged += (v, v1) =>
			{
				vv.Refresh();
			};
		}
    }
}
