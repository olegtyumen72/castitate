﻿using Castitate.GUI.Model;
using Castitate.GUI.ViewModel;
using Castitate.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Castitate.GUI.Views
{
	/// <summary>
	/// Interaction logic for SettingsView.xaml
	/// </summary>
	public partial class SettingsView : UserControl
	{
		public SettingsView()
		{
			InitializeComponent();
			DataContext = new SettingsInfo();
		}

		public SettingsView(SettingsInfo si)
		{
			InitializeComponent();
			DataContext = si;
			//si.Refresh = () => WhiteLst.UCDataSource = si.Data.WhiteList;
			WhiteLst.UCDataSource = si.Data.WhiteList;
			si.PropertyChanged += Si_PropertyChanged;

			WhiteLst.RemoveElement += WhiteLst_RemoveElement;

			Loaded += (v, v1) =>
			{
				si.PropertyChanged += (obj, args) =>
				{
					var tmp = obj as SettingsInfo;
					if (tmp != null && args.PropertyName == "DaysToDie")
					{
						if(MessageBox.Show("Количество дней до устаревания изменилось. Данные возможно не корректны. Обновить?", "Внимание" ,MessageBoxButton.OKCancel, MessageBoxImage.Warning) == MessageBoxResult.OK)
						{
							VkCachManager.Instance.HardUpdate();
						}
					}
				};
			};
		}

		private void WhiteLst_RemoveElement(object sender, ListBox e)
		{
			if(e.SelectedItem != null)
			{
				var tmp = DataContext as SettingsInfo;
				tmp.Data.WhiteList.Remove(e.SelectedItem as ImageTextHolder);
				WhiteLst.UCDataSource = tmp.Data.WhiteList;
			}
		}

		private void Si_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			var tmp = sender as SettingsInfo;
			if(tmp != null && e.PropertyName == "Data.WhiteList")
			{
				WhiteLst.UCDataSource = tmp.Data.WhiteList;
			}
		}
	}
}
