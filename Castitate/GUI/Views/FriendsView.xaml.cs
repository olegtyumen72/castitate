﻿using Castitate.GUI.Model;
using Castitate.GUI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Castitate.GUI.Views
{
	/// <summary>
	/// Interaction logic for FriendsView.xaml
	/// </summary>
	public partial class FriendsView : UserControl
	{
		public FriendsView()
		{
			InitializeComponent();
			DataContext = new FriendsViewModel();
			var vv = DataContext as FriendsViewModel;

			vv.AttachFilter(
				(str) =>
				{
					try
					{
						var el = str as FriendInf;
						var text = (CombFilter.SelectedItem as TextBlock).Text;
						switch (text)
						{
							case "None":
								{
									return true;
								}
							case "Soon":
								{
									if (el.State == Logic.Interfaces.VDBState.Soon) return true;
									break;
								}
							case "Passed":
								{
									if (el.State == Logic.Interfaces.VDBState.Passed) return true;
									break;
								}
							case "Future":
								{
									if (el.State == Logic.Interfaces.VDBState.Future) return true;
									break;
								}
						}
						return false;
					}
					catch
					{
						return false;
					}
				});

			CombFilter.SelectionChanged += (v, v1) =>
			{
				vv.Refresh();
			};
		}
	}
}
