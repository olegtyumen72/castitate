﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Castitate.GUI.Views
{
	/// <summary>
	/// Логика взаимодействия для SmartList.xaml
	/// </summary>
	public partial class SmartList : UserControl
	{

		public event EventHandler<ListBox> AddElement;
		public event EventHandler<ListBox> RemoveElement;

		public SmartList()
		{

			InitializeComponent();

		}


		public string TextInTxtBlck
		{
			get;
			set;
		}
		public string TextInTxtBx
		{
			get;
			set;
		}

		IEnumerable<string> _uCDataSource;

		public IEnumerable<string> UCDataSource
		{
			get
			{
				return _uCDataSource;
			}
			set
			{
				_uCDataSource = value;
				var viewElements = CollectionViewSource.GetDefaultView(value);
				viewElements.Filter = (str) =>
				{
					try
					{
						return (str as string).Contains(TextInTxtBx);
					}
					catch
					{
						return true;
					}
				};
				viewElements.GroupDescriptions.Add(new PropertyGroupDescription("Type"));
				Spisok.ItemsSource = viewElements;
			}
		}

		private void ClickAdd(object sender, RoutedEventArgs e)
		{
			AddElement?.Invoke(this, Spisok);
		}
		private void ClickRemove(object sender, RoutedEventArgs e)
		{
			RemoveElement?.Invoke(this, Spisok);
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (Spisok.ItemsSource != null)
				(Spisok.ItemsSource as ICollectionView).Refresh();
		}
	}



}
