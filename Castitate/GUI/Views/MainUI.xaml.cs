﻿using Castitate.Logic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Castitate.GUI
{
	/// <summary>
	/// Interaction logic for MainUI.xaml
	/// </summary>
	public partial class MainUI : UserControl
	{

		ObservableCollection<string> _menus = new ObservableCollection<string>();
		static RoutedUICommand ClickMenu = new RoutedUICommand(" ", "ClicMenu", typeof(MainUI));
		

		public event EventHandler<string> MenuClick;

		public static RoutedUICommand Click
		{
			get { return ClickMenu; }
		}

		public string ImagePath
		{
			get;
			set;
		}

		static MainUI()
		{
			
			
		}

		public MainUI()
		{
			InitializeComponent();
			
			//_menus = new ObservableCollection<string>() { "Groups", "Friends", "Events", "Settings" };
			//MainMenu.ItemsSource = _menus;
		}

		
	}
}
