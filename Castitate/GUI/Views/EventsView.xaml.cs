﻿using Castitate.GUI.Model;
using Castitate.GUI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace Castitate.GUI.Views
{
	public partial class EventsView : UserControl
	{
		public EventsView()
		{
			InitializeComponent();
			DataContext = new EventsViewModel();
			var vv = DataContext as EventsViewModel;

			vv.AttachFilter(
				(str) =>
			{
				try
				{
					var el = str as EventInfo;
					var text = (CombFilter.SelectedItem as TextBlock).Text;
					switch (text)
					{
						case "None":
							{
								return true;
							}
						case "Soon":
							{
								if (el.State == Logic.Interfaces.VDBState.Soon) return true;
								break;
							}
						case "Passed":
							{
								if (el.State == Logic.Interfaces.VDBState.Passed) return true;
								break;
							}
						case "Future":
							{
								if (el.State == Logic.Interfaces.VDBState.Future) return true;
								break;
							}
					}
					return false;
				}
				catch
				{
					return false;
				}
			});

			CombFilter.SelectionChanged += (v, v1) =>
			{
				vv.Refresh();
			};
		}
	}
}
