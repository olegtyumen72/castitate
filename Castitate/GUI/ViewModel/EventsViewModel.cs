﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Linq;
using Castitate.GUI.Model;
using Castitate.GUI.Helpers;
using Castitate.Logic.DataClasses;
using Castitate.Logic;
using Castitate.Logic.Interfaces;
using System.Windows.Threading;
using System.ComponentModel;
using System.Windows.Data;

namespace Castitate.GUI.ViewModel
{
	public class EventsViewModel : ViewModelBase
	{
		ObservableCollection<EventInfo> _events;

		IEnumerable<VkDataBase> evs;

		public EventsViewModel()
		{
			Events = new ObservableCollection<EventInfo>();


			VkManager.Instance.EventService.DataUpdated += (v) =>
			{
				///ThreadSafe
				MainWindowHolder.Disp.Invoke(() =>
				{
					
					evs = v;
					Events.Clear();
					foreach (var item in evs)
					{
						var ev = (item as VkDataBase);						
						Events.Add(new EventInfo(ev));
					}
					LocalRefresh();
					Refresh();
				});
			};

			evs = VkManager.Instance.EventService.GetAllData();
			if (evs != null && evs.Count() > 0)
			{
				evs.ToList().ForEach((v) => Events.Add(new EventInfo(v)));
				LocalRefresh();
				Refresh();
			}
			//VkCachManager.Instance.HardUpdate();
		}

		public ObservableCollection<EventInfo> Events
		{
			get
			{
				return _events;
			}
			set
			{
				_events = value;
				View = CollectionViewSource.GetDefaultView(value);
				OnPropertyChanged("Events");
			}
		}	
		

		public int OldCount
		{
			set;
			get;
		}

		ICommand _removeCommand;
		public ICommand RemoveCommand
		{
			get
			{
				if (_removeCommand == null)
				{
					_removeCommand = new RelayCommand(RemoveGroup);
				}
				return _removeCommand;
			}
		}

		ICommand _removeSelectedCommand;
		public ICommand RemoveSelectedCommand
		{
			get
			{
				if (_removeSelectedCommand == null)
				{
					_removeSelectedCommand = new RelayCommand(RemoveSelectedGroup);
				}
				return _removeSelectedCommand;
			}
		}

		ICommand _removeOld;

		public ICommand RemoveOld
		{
			get
			{
				if (_removeOld == null)
				{
					_removeOld = new RelayCommand(RemoveOldA);
				}
				return _removeOld;
			}
		}

		void LocalRefresh()
		{
			OldCount = Events.Where((v) => v.State == VDBState.Passed).Count();
		}

		private async void RemoveOldA(object param)
		{
			var to_del = new List<VkDataBase>();
			foreach(var item in Events.ToList())
			{
				if (item.State == VDBState.Passed && VkManager.Instance.CastSettings.Data.WhiteList.Any((v) => v.ID != item.ID))
				{
					to_del.Add(evs.First((v) => v.ID == item.ID));
				}
			}

			if (to_del.Count > 0)
			{
				to_del.ForEach((v) => Events.Remove(Events.First((c) => c.ID == v.ID)));
				await VkManager.Instance.EventService.Removed(to_del);
				View = CollectionViewSource.GetDefaultView(Events);
				Refresh();
				LocalRefresh();
			}
		}

		private async void RemoveGroup(object parameter)
		{
			int index = Events.IndexOf(parameter as EventInfo);
			if (index > -1 && index < Events.Count)
			{
				var to_delete = new List<VkDataBase>();
				foreach (var item in evs)
				{
					if ((parameter as EventInfo).ID == item.ID)
					{
						to_delete.Add(item);
					}
				}

				await VkManager.Instance.EventService.Removed(to_delete);
				Events.RemoveAt(index);
				
			}
			View = CollectionViewSource.GetDefaultView(Events);
			Refresh();
			LocalRefresh();
		}

		private async void RemoveSelectedGroup(object parameter)
		{
			var to_delete = new List<VkDataBase>();
			foreach (var item in Events.ToList())
			{
				if (item.IsChecked)
				{					
					foreach (var ev in evs)
					{
						if (item.ID == ev.ID)
						{
							to_delete.Add(ev);
						}
					}					
					int index = Events.IndexOf(item);
					Events.RemoveAt(index);
				}
			}
			if (to_delete.Count > 0)
			{
				await VkManager.Instance.EventService.Removed(to_delete);
				View = CollectionViewSource.GetDefaultView(Events);
				Refresh();
				LocalRefresh();
			}
		}


		ICommand _addToWhite;

		public ICommand AddToWhite
		{
			get
			{
				if (_addToWhite == null)
				{
					_addToWhite = new RelayCommand(AddToWhiteS);
				}
				return _addToWhite;
			}
		}

		void AddToWhiteS(object parameter)
		{
			var tmp = parameter as EventInfo;
			VkManager.Instance.CastSettings.Data.WhiteList.Add(new ImageTextHolder()
			{
				Text = tmp.Title,
				Tag = $"Event:{tmp.ID}",
				ImageSource = tmp.ImagePath,
				ID = tmp.ID
			});
			VkManager.Instance.CastSettings.OnPropertyChanged("Data.WhiteList");
		}
	}
}
