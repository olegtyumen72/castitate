﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Linq;
using Castitate.GUI.Model;
using Castitate.GUI.Helpers;
using Castitate.Logic.DataClasses;
using Castitate.Logic;
using Castitate.Logic.Interfaces;
using System.Windows.Threading;
using System.ComponentModel;
using System.Windows.Data;

namespace Castitate.GUI.ViewModel
{
	class FriendsViewModel: ViewModelBase
	{

		ObservableCollection<FriendInf> _friends;

		IEnumerable<VkDataBase> _frd;

		public FriendsViewModel()
		{
			Friends = new ObservableCollection<FriendInf>();


			VkManager.Instance.FriendService.DataUpdated += (v) =>
			{
				///ThreadSafe
				MainWindowHolder.Disp.Invoke(() =>
				{
					_frd = v;
					Friends.Clear();
					foreach (var item in _frd)
					{
						var ev = (item as VkDataBase);
						Friends.Add(new FriendInf(ev));
					}
					LocalRefresh();
					Refresh();
				});
			};

			_frd = VkManager.Instance.FriendService.GetAllData();
			if (_frd != null && _frd.Count() > 0)
			{
				_frd.ToList().ForEach((v) => Friends.Add(new FriendInf(v)));
				LocalRefresh();
				Refresh();
			}
			//VkCachManager.Instance.HardUpdate();
		}

		public ObservableCollection<FriendInf> Friends
		{
			get
			{
				return _friends;
			}
			set
			{
				_friends = value;
				View = CollectionViewSource.GetDefaultView(value);
				OnPropertyChanged("Events");
			}
		}


		public int OldCount
		{
			set;
			get;
		}

		ICommand _removeCommand;
		public ICommand RemoveCommand
		{
			get
			{
				if (_removeCommand == null)
				{
					_removeCommand = new RelayCommand(RemoveGroup);
				}
				return _removeCommand;
			}
		}

		ICommand _removeSelectedCommand;
		public ICommand RemoveSelectedCommand
		{
			get
			{
				if (_removeSelectedCommand == null)
				{
					_removeSelectedCommand = new RelayCommand(RemoveSelectedGroup);
				}
				return _removeSelectedCommand;
			}
		}

		ICommand _removeOld;

		public ICommand RemoveOld
		{
			get
			{
				if (_removeOld == null)
				{
					_removeOld = new RelayCommand(RemoveOldA);
				}
				return _removeOld;
			}
		}

		void LocalRefresh()
		{
			OldCount = Friends.Where((v) => v.State == VDBState.Passed).Count();
		}

		private async void RemoveOldA(object param)
		{
			var to_del = new List<VkDataBase>();
			foreach (var item in Friends.ToList())
			{
				if (item.State == VDBState.Passed && VkManager.Instance.CastSettings.Data.WhiteList.Any((v) => v.ID != item.ID))
				{
					to_del.Add(_frd.First((v) => v.ID == item.ID));
				}
			}
			if (to_del.Count > 0)
			{
				to_del.ForEach((v) => Friends.Remove(Friends.First((c) => c.ID == v.ID)));
				await VkManager.Instance.FriendService.Removed(to_del);
				View = CollectionViewSource.GetDefaultView(Friends);
				Refresh();
				LocalRefresh();
			}
		}

		private async void RemoveGroup(object parameter)
		{
			int index = Friends.IndexOf(parameter as FriendInf);
			if (index > -1 && index < Friends.Count)
			{
				var to_delete = new List<VkDataBase>();
				foreach (var item in _frd)
				{
					if ((parameter as FriendInf).ID == item.ID)
					{
						to_delete.Add(item);
					}
				}

				await VkManager.Instance.FriendService.Removed(to_delete);
				Friends.RemoveAt(index);

			}
			View = CollectionViewSource.GetDefaultView(Friends);
			Refresh();
			LocalRefresh();
		}

		private async void RemoveSelectedGroup(object parameter)
		{
			var to_delete = new List<VkDataBase>();
			foreach (var item in Friends.ToList())
			{
				if (item.IsChecked)
				{
					foreach (var ev in _frd)
					{
						if (item.ID == ev.ID)
						{
							to_delete.Add(ev);
						}
					}
					int index = Friends.IndexOf(item);
					Friends.RemoveAt(index);
				}
			}
			if (to_delete.Count > 0)
			{
				await VkManager.Instance.FriendService.Removed(to_delete);
				View = CollectionViewSource.GetDefaultView(Friends);
				Refresh();
				LocalRefresh();
			}
		}

		ICommand _addToWhite;

		public ICommand AddToWhite
		{
			get
			{
				if (_addToWhite == null)
				{
					_addToWhite = new RelayCommand(AddToWhiteS);
				}
				return _addToWhite;
			}
		}

		void AddToWhiteS(object parameter)
		{
			var tmp = parameter as FriendInf;
			VkManager.Instance.CastSettings.Data.WhiteList.Add(new ImageTextHolder()
			{
				Text = tmp.Title,
				Tag = $"Friend:{tmp.ID}",
				ImageSource = tmp.ImagePath,
				ID = tmp.ID
			});
			VkManager.Instance.CastSettings.OnPropertyChanged("Data.WhiteList");
		}
	}
}
