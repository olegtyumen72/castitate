﻿using Castitate.GUI.Model;
using Castitate.GUI.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Castitate.GUI.ViewModel
{
	public class SettingsInfo: ViewModelBase
	{

		SettingsData _data = new SettingsData();

		public SettingsData Data
		{
			get { return _data; }
			set { _data = value;
				OnPropertyChanged("Data.WhiteList");
			}
		}

		//public Action Refresh = null;
	}
}
