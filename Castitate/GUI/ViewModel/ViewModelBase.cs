﻿using System;
using System.ComponentModel;

namespace Castitate.GUI.ViewModel
{
	
    public class ViewModelBase : INotifyPropertyChanged
    {
        public ViewModelBase()
        {

        }

        public void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

		public void Refresh()
		{
			View.Refresh();
		}

		ICollectionView _view;

		public ICollectionView View
		{
			get { return _view; }
			set { _view = value; }
		}

		public void AttachFilter(Predicate<object> str)
		{
			View.Filter = str;
		}		

		public event PropertyChangedEventHandler PropertyChanged;
    }
}
