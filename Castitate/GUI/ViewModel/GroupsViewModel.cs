﻿using System;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Linq;
using Castitate.GUI.Model;
using Castitate.GUI.Helpers;
using Castitate.Logic;
using Castitate.Logic.DataClasses;
using Castitate.Logic.Interfaces;
using System.Windows.Data;
using System.Collections.Generic;

namespace Castitate.GUI.ViewModel
{
    public class GroupsViewModel : ViewModelBase
    {
        ObservableCollection<Groups> _groups;
		List<VkDataBase> _grs = new List<VkDataBase>();

        public GroupsViewModel()
        {
		
			Groups = new ObservableCollection<Groups>();

			VkManager.Instance.GroupService.DataUpdated += (v) =>
			{
				///ThreadSafe
				MainWindowHolder.Disp.Invoke(() =>
				{
					_grs = v.ToList();
					Groups.Clear();
					foreach (var item in v)
					{
						var ev = (item as VkGroupData);
						_groups.Add(new Groups(ev));
					}
					Refresh();
					LocalRefresh();
				});
			};			

			var groups = VkManager.Instance.GroupService.GetAllData();
			if (groups != null && groups.Count() > 0)
			{
				groups.ToList().ForEach((v) => { Groups.Add(new Groups(v as VkDataBase)); _grs.Add(v); });
				Refresh();
				LocalRefresh();
			}
		}		

		public ObservableCollection<Groups> Groups
        {
            get
            {
                return _groups;
            }
            set
            {
                _groups = value;
				View = CollectionViewSource.GetDefaultView(value);
				OnPropertyChanged("Groups");
            }
        }

		public int OldCount
		{
			set;
			get;
		}

		void LocalRefresh()
		{
			OldCount = Groups.Where((v) => v.State == VDBState.Passed).Count();
		}

		ICommand _removeOld;

		public ICommand RemoveOld
		{
			get
			{
				if (_removeOld == null)
				{
					_removeOld = new RelayCommand(RemoveOldA);
				}
				return _removeOld;
			}
		}		

		private async void RemoveOldA(object param)
		{
			var to_del = new List<VkDataBase>();
			foreach (var item in Groups.ToList())
			{
				if (item.State == VDBState.Passed && VkManager.Instance.CastSettings.Data.WhiteList.Any((v) => v.ID != item.ID))
				{
					to_del.Add(_grs.First((v) => v.ID == item.ID));
				}
			}

			if (to_del.Count > 0)
			{
				to_del.ForEach((v) => Groups.Remove(Groups.First((c) => c.ID == v.ID)));
				await VkManager.Instance.EventService.Removed(to_del);
				View = CollectionViewSource.GetDefaultView(Groups);
				Refresh();
				LocalRefresh();
			}
		}

		ICommand _removeCommand;
        public ICommand RemoveCommand
        {
            get
            {
                if (_removeCommand == null)
                {
                    _removeCommand = new RelayCommand(RemoveGroup);
                }
                return _removeCommand;
            }
        }

        ICommand _removeSelectedCommand;
        public ICommand RemoveSelectedCommand
        {
            get
            {
                if (_removeSelectedCommand == null)
                {
                    _removeSelectedCommand = new RelayCommand(RemoveSelectedGroup);
                }
                return _removeSelectedCommand;
            }
        }

        private async void RemoveGroup(object parameter)
        {
            int index = Groups.IndexOf(parameter as Groups);
            if (index > -1 && index < Groups.Count)
            {
                Groups.RemoveAt(index);
				await VkManager.Instance.GroupService.Removed(new VkDataBase[] { _grs[index] });
            }
			
			View = CollectionViewSource.GetDefaultView(Groups);
			Refresh();
			LocalRefresh();
		}

        private async void RemoveSelectedGroup(object parameter)
        {
			var delList = new List<VkDataBase>();
            foreach (var item in Groups.ToList())
            {
				if (item.IsChecked)
				{
					delList.Add(_grs[Groups.IndexOf(item)]);
					Groups.Remove(item);					
				}
			}
			if (delList.Count > 0)
			{
				await VkManager.Instance.GroupService.Removed(delList);
				View = CollectionViewSource.GetDefaultView(Groups);
				Refresh();
				LocalRefresh();
			}
		}

		ICommand _addToWhite;

		public ICommand AddToWhite
		{
			get
			{
				if (_addToWhite == null)
				{
					_addToWhite = new RelayCommand(AddToWhiteS);
				}
				return _addToWhite;
			}
		}

		void AddToWhiteS(object parameter)
		{
			var tmp = parameter as Groups;
			VkManager.Instance.CastSettings.Data.WhiteList.Add(new ImageTextHolder()
			{
				Text = tmp.Title,
				Tag = $"Group:{tmp.ID}",
				ImageSource = tmp.ImagePath,
				ID = tmp.ID
			});
			VkManager.Instance.CastSettings.OnPropertyChanged("Data.WhiteList");
		}
	}
}
