﻿using Castitate.GUI.Views;
using Castitate.Logic;
using Castitate.Logic.Entity;
using Castitate.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Castitate.GUI
{
	/// <summary>
	/// Interaction logic for MainWindowHolder.xaml
	/// </summary>
	public partial class MainWindowHolder : Window
	{
		/// <summary>
		/// Нужен для обработки данных из другого потока
		/// </summary>
		public static Dispatcher Disp = Dispatcher.CurrentDispatcher;

		private static Dictionary<string, UIElement> controls = new Dictionary<string, UIElement>();

		public MainWindowHolder()
		{
			InitializeComponent();
			controls.Add(nameof(EventsView), new EventsView());
			controls.Add(nameof(GroupsView), new GroupsView());
			controls.Add(nameof(FriendsView), new FriendsView());
			controls.Add(nameof(SettingsView), new SettingsView(VkManager.Instance.CastSettings));

			CommandBinding ClickBind = new CommandBinding(MainUI.Click);
			ClickBind.Executed += MenuClick;
			CommandBindings.Add(ClickBind);

			//MainUIo.ImagePath = VkManager.Instance.Image;

			Loaded += (o, a) =>
			{
				UIHolder.DataContext = controls["EventsView"];
				UIHolder.Children.Add(controls["EventsView"]);
				curElem = controls["EventsView"];
				//VkCachManager.Instance.HardUpdate();
			};


			Closed += (o, a) =>
			{
				Task.Run(() =>
			   {
				   
				   while (VkManager.Instance.EventService.IsBusy() ||
					 VkManager.Instance.FriendService.IsBusy() ||
					  VkManager.Instance.GroupService.IsBusy())
				   {
					   Thread.Sleep(1000);
				   }
				   Disp.InvokeShutdown();
			   });
			};

		}

		UIElement curElem = null;

		private void MenuClick(object sender, ExecutedRoutedEventArgs e)
		{
			var text = (e.OriginalSource as TextBlock).Text;
			UIHolder.Children.Clear();

			switch (text)
			{
				case "Друзья":
					{
						UIHolder.Children.Add(controls[nameof(FriendsView)]);
						curElem = controls[nameof(FriendsView)];
						break;
					}
				default:
				case "События":
					{
						UIHolder.Children.Add(controls[nameof(EventsView)]);
						curElem = controls[nameof(EventsView)];
						break;
					}
				case "Настройки":
					{
						UIHolder.Children.Add(controls[nameof(SettingsView)]);
						curElem = controls[nameof(SettingsView)];
						break;
					}
				case "Группы":
					{
						UIHolder.Children.Add(controls[nameof(GroupsView)]);
						curElem = controls[nameof(GroupsView)];
						break;
					}
				case "Выход":
					{
						Close();
						break;
					}
				case "Сменить ак.":
					{
						Hide();
						break;
					}
				case "Обновить":
					{
						VkCachManager.Instance.HardUpdate();
						UIHolder.Children.Add(curElem);
						break;
					}
			}
		}
	}
}
