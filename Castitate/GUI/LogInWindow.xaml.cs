﻿
using Castitate.GUI.Model;
using Castitate.GUI.ViewModel;
using Castitate.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Castitate.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class LogInWindow : Window
    {      



        public LogInWindow()
        {
            InitializeComponent();
			var cur = Dispatcher.CurrentDispatcher;
			if (VkCachManager.Instance.IsFileExist("Settings"))
			{
				VkManager.Instance.CastSettings.Data = VkCachManager.Instance.Load("Settings").First() as SettingsData;
			}

			if (VkManager.Instance.CastSettings.Data.IsSaveMainData && VkCachManager.Instance.IsFileExist("LogFile"))
			{
				var res = VkCachManager.Instance.Load("LogFile");
				LogUC.Login = res.First() as string;
				LogUC.Password = res.Last() as string;				
			}

			LogUC.IsRememberMe = VkManager.Instance.CastSettings.Data.IsSaveMainData;


			Closed += (v, v1) =>
			{
				cur.InvokeShutdown();
			};
		}

		bool IsRun = false;

        private async void LogInUC_LoginClick(object sender, RoutedEventArgs e)
        {
			if (!IsRun)
			{
				IsRun = true;
				try
				{
					await VkManager.Instance.InitVkManager(LogUC.Login, LogUC.Password);

					if (VkManager.Instance.IsAuto)
					{
						LogUC.MistakeText = "";
						VkManager.Instance.CastSettings.Data.IsSaveMainData = LogUC.IsRememberMe;
						var win = (new MainWindowHolder());

						win.Closed += (v, v1) =>
						{

							VkCachManager.Instance.Save("Settings", new[] { VkManager.Instance.CastSettings.Data });

							if (VkManager.Instance.CastSettings.Data.IsSaveMainData)
							{
								VkCachManager.Instance.Save("LogFile", new[] { LogUC.Login, LogUC.Password });
							}
							else if (VkCachManager.Instance.IsFileExist("LogFile"))
							{
								VkCachManager.Instance.RemoveFile("LogFile");
							}

							Close();
						};

						Hide();
						win.Show();
						IsRun = false;

					}
					else
					{
						LogUC.MistakeText = "Can't log in. Check login and password.";
						IsRun = false;
					}
				}
				catch(VkNet.Exception.VkApiAuthorizationException exp)
				{
					LogUC.MistakeText = "Can't log in. Check login and password.";
					IsRun = false;
				}
				catch (VkNet.Exception.UserAuthorizationFailException exp)
				{
					LogUC.MistakeText = "Can't log in. Check login and password.";
					IsRun = false;
				}			
				catch (VkNet.Exception.VkApiException exp)
				{
					LogUC.MistakeText = "Can't log in. Check internet connection";
					IsRun = false;
				}
			}
        }
    }
}
