﻿using Castitate.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using VkNet.Model;

namespace Castitate.Logic.DataClasses
{
    /// <summary>
    /// Описание смотри в Interfaces/VkDataBase.cs
    /// </summary>
	[Serializable]
    public class VkEventData : VkDataBase
    {
        public VkEventData(Group elem)
        {
            Name = elem.Name;
            if (elem.StartDate != null)
            {
                Date = elem.StartDate.Value;
            }
            else
            {
                Date = DateTime.Now;
            }
            ImagePath = elem.Photo50.ToString();
			State = DateComparer(DateTime.Now);
			Details = new Dictionary<string, object>
            {
                { "State",  State },
                { "MembersCount", elem.MembersCount}
            };
			
			ID = elem.Id;
        }

    }
}
