﻿using Castitate.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using VkNet.Enums.SafetyEnums;
using VkNet.Model;
using Castitate.Logic.Helpers;

namespace Castitate.Logic.DataClasses
{
	/// <summary>
	/// Описание смотри в Interfaces/VkDataBase.cs
	/// </summary>
	[Serializable]
	public class VkGroupData : VkDataBase
    {

        public VkGroupData(Group elem)
        {

            Name = elem.Name;


            ImagePath = elem.Photo50.ToString();
			int realCount = elem.MembersCount ?? -1;


			if (elem.MembersCount == null || elem.Deactivated.ToVDBStatus(realCount) == VDBStatus.Dead)
            {
                Date = DateTime.Now.AddYears(-2000);
            }
            else
            {
                Date = VkManager.Instance.PostService.GetLastActiveDataGroup(elem.Id, elem, DateTime.Now);
            }

			State = DateComparer(DateTime.Now);
			Details = new Dictionary<string, object>
            {
                { "State", DateComparer(DateTime.Now) },
                { "MembersCount", elem.MembersCount},
                { "Status", elem.Deactivated.ToVDBStatus(realCount) },
               // { "Ban", "" },
                {"ScreenName", elem.ScreenName }

            };           

            ID = elem.Id;
        }

    }
}
