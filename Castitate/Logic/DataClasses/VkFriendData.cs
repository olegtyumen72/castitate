﻿using Castitate.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VkNet.Model;

namespace Castitate.Logic.DataClasses
{
	/// <summary>
	/// Описание смотри в Interfaces/VkDataBase.cs
	/// </summary>
	[Serializable]
	public class VkFriendData: VkDataBase
	{

        public VkFriendData(User usr) 
        {
            Name = $"{usr.LastName} {usr.FirstName}";
            ImagePath = usr.Photo50.ToString();

            if (usr.IsDeactivated)
            {
                Date = DateTime.Now.AddYears(-2000);
            }
            else
            {
                Date = VkManager.Instance.PostService.GetLastActiveDataFriend(usr.Id, usr, DateTime.Now);
            }

			State = DateComparer(DateTime.Now);
			Details = new Dictionary<string, object>
            {
				{ "MembersCount", usr.FollowersCount ?? 0 },
                { "State", DateComparer(DateTime.Now) },                
                { "Status", usr.Deactivated },
                {"ScreenName", usr.ScreenName }

            };

            ID = usr.Id;
        }

	}
}
