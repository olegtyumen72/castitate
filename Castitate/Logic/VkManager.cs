﻿using Castitate.GUI;
using Castitate.GUI.Model;
using Castitate.GUI.ViewModel;
using Castitate.Logic.Entity;
using Castitate.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Exception;
using VkNet.Model.RequestParams;

namespace Castitate.Logic
{
    /// <summary>
    /// МОЗГ! ОН ТУТ!
    /// </summary>
    public class VkManager
    {

		protected VkManager() { }

		private sealed class VkManagerCreater
		{
			public static VkManager Instance { get; } = new VkManager();
		}

		public static VkManager Instance
		{
			get { return VkManagerCreater.Instance; }
		}

        #region Private Personal, DO NOT TOUCH!

        private static string log = "";
        private static string pass = "";

        #endregion

        /// <summary>
        /// ID приложения в dev в VK
        /// </summary>
        private ulong appID = 5920878;
        /// <summary>
        /// Мысли в моей голове...
        /// </summary>
        private VkApi vk = new VkApi();

		public SettingsInfo CastSettings = new SettingsInfo();		

		VkApi VK
        {
            get { return vk; }
        }

        /// <summary>
        /// Рука
        /// </summary>
        public IVkServiceProvider GroupService;
        /// <summary>
        /// Нога
        /// </summary>
        public IVkServiceProvider EventService;
        /// <summary>
        /// Рот
        /// </summary>
        public IVkServiceProvider FriendService;
        /// <summary>
        /// Пальца-языка
        /// </summary>
        public VkDataProvider PostService;

        /// <summary>
        /// Хэй парень, а у тебя есть авторизация?
        /// </summary>
        public bool IsAuto
        {
            get { return vk.IsAuthorized; }
        }

        /// <summary>
        /// МОЗГ, работай!
        /// </summary>
        /// <returns></returns>
        public async Task<bool> InitVkManager()
        {
            return await InitVkManager(log, pass);
        }

        /// <summary>
        /// Работай я сказал!
        /// </summary>
        /// <param name="loging"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<bool> InitVkManager(string loging, string password)
        {
            var holder = new CapchaHolder();
            long capID = -1;
            string cap = "";

            while (vk == null || !vk.IsAuthorized)
            {
                try
                {
                    log = loging;
                    pass = password;
                    await InitVk(capID, cap);                                        
                    if (vk.IsAuthorized)
                    {
                       await InitServices();
						//VkCachManager.Instance.ElGigle();
                        //(new Task(() => SimpleVkCachManager.HardUpdate())).Start();  
                    }
                    return vk.IsAuthorized;
                }
                catch (CaptchaNeededException e)
                {

                    capID = e.Sid;

                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.UriSource = e.Img;
                    bitmap.EndInit();

                    holder.CapchaPic.Source = bitmap;

                    holder.ShowDialog();


                    cap = holder.TextHolder.Text;

                }
            }

            return false;
        }

        /// <summary>
        /// Подключили руки-ноги к мозгу
        /// </summary>
        private async Task InitServices()
        {
			await Task.Run(() =>
		   {
			   PostService = new VkDataProvider(vk.Wall, vk.Docs, vk.Audio, vk.Messages);
			   EventService = new EventServiceProvider(vk.Groups, vk.UserId.Value);
			   GroupService = new GroupServiceProvider(vk.Groups, vk.UserId.Value);
			   FriendService = new FriendServiceProvider(vk.Friends, vk.UserId.Value);

			   VkCachManager.Instance.InitCache();
		   });
        }

        /// <summary>
        /// Рождаем мысли
        /// </summary>
        /// <param name="capchaID"></param>
        /// <param name="cpchAnswer"></param>
       async Task InitVk(long capchaID = -1, string cpchAnswer = "")
        {
            if (capchaID != -1)
            {
                await vk.AuthorizeAsync(new ApiAuthParams()
                {
                    ApplicationId = appID,
                    Login = log,
                    Password = pass,
                    Settings = Settings.All,
                    CaptchaSid = capchaID,
                    CaptchaKey = cpchAnswer
                });
            }
            else
            {
               await vk.AuthorizeAsync(new ApiAuthParams()
                {
                    ApplicationId = appID,
                    Login = log,
                    Password = pass,
                    Settings = Settings.All,
                });
            }
        }

    }
}
