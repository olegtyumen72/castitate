﻿using Castitate.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VkNet.Enums.SafetyEnums;
using VkNet.Model.RequestParams;

namespace Castitate.Logic.Helpers
{

    /// <summary>
    /// Вспомогательный класс
    /// </summary>
    public static class Booster
    {

        public static GroupsGetParams Copy(this GroupsGetParams value)
        {
            return new GroupsGetParams()
            {
                UserId = value.UserId,
                Filter = value.Filter,
                Extended = value.Extended,
                Fields = value.Fields,
                Count = value.Count,
                Offset = value.Offset
            };
        }

        public static FriendsGetParams Copy(this FriendsGetParams value)
        {
            return new FriendsGetParams()
            {
                Count = value.Count,
                Fields = value.Fields,
                ListId = value.ListId,
                NameCase = value.NameCase,
                Offset = value.Offset,
                Order = value.Order,
                UserId = value.UserId
            };
        }

        public static VDBStatus ToVDBStatus(this Deactivated value, int memCount)
        {
            if (value == null && memCount <= 0) return VDBStatus.Dead;
			if (value == null) return VDBStatus.Active;
            switch (value.ToString())
            {
                case "Activated":
                    {
                        return VDBStatus.Active;
                    }
                default:
                    {
                        return VDBStatus.Dead;
                    }
            }
        }

    }
}
