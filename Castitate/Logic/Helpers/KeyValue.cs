﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Castitate.Logic.Helpers
{
    /// <summary>
    /// Вспомогательный класс для серализации дикшинари
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    [Serializable]
    public class KeyValue<TKey,TValue>
    {
        public TKey Key { get; set; }
        public TValue Value { get; set; }

    }
}
