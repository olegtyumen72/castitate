﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Timers;
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using Castitate.Logic.Helpers;
using Castitate.Logic.Interfaces;
using System.Windows.Threading;
using System.Threading.Tasks;
using Castitate.Logic.Entity;

namespace Castitate.Logic
{
	class VkCachManager
	{
		/// <summary>
		/// Ивент для сервисов
		/// </summary>
		public event Action<Dictionary<string, object>> UpdateData;
		/// <summary>
		/// Костыль, срабатывает при загрузке данныех из кэша
		/// </summary>
		public event Action<Dictionary<string, object>> DataLoaded;

		private sealed class VkChachManagerCreater
		{
			public static VkCachManager Instance { get; } = new VkCachManager();
		}

		public static VkCachManager Instance
		{
			get { return VkChachManagerCreater.Instance; }
		}

		protected VkCachManager()
		{

		}

		public bool IsFileExist(string fileName)
		{
			if (!Directory.Exists(mainPath)) return false;
			return Directory.GetFiles(mainPath).Any((v) => Path.GetFileNameWithoutExtension(v) == fileName);
		}

		public void InitCache()
		{
			var files = GetCachedFilesNames();			
			var servs = CheckServiceFiles(files);
			var times = GetCacheTime(files);

			if (servs.All((v) => v.Value == true))
			{
				if (LoadCache())
					DataLoaded?.Invoke(CachDic);
				else
					UpdateCache(null, null);
			}
			else if (servs.All((v) => v.Value == false))
			{
				UpdateCache(null, null);
			}
			else
			{
				for (int i = 0; i < servs.Count; i++)
				{
					var serv_state = servs.ElementAt(i);
					if (serv_state.Value)
					{
						if (Math.Abs((times[serv_state.Key] - DateTime.Now).TotalHours) >= HoursToDie)
						{
							UpdateCache(null, null);
							break;
						}
						else
						{
							if (!LoadCache(serv_state.Key))
							{
								UpdateCache(null, null);
								break;
							}
						}
					}
				}
				DataLoaded?.Invoke(CachDic);
				UpdateCache(null, null);
			}
			Timer.Interval = TimeSpan.FromHours(HoursToDie).TotalMilliseconds;
			//Тестовые 30 секунд
			//Timer.Interval = TimeSpan.FromSeconds(30).TotalMilliseconds;
			Timer.Elapsed += UpdateCache;
			Timer.Start();
		}

		private Dictionary<string, bool> CheckServiceFiles(IEnumerable<string> files)
		{
			var servs = new List<string>() {
				nameof(EventServiceProvider),
				nameof(GroupServiceProvider),
				nameof(FriendServiceProvider)
			};
			var dic = new Dictionary<string, bool>();

			foreach (var srvs in servs)
			{
				dic.Add(srvs, files.Contains((srvs)));
			}

			return dic;
		}

		public void RemoveFile(string v)
		{
			File.Delete(Path.Combine(mainPath, v));
		}

		bool LoadCache(string file)
		{
			try
			{
				CachDic.Add(file, Load(file));
				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool LoadCache()
		{
			var files = GetCachedFilesNames();
			if (files.Count() <= 0) return false;
			//if (files.Count() == 1 && files.Any((v) => Path.GetFileNameWithoutExtension(v) == "LogFile")) return false;
			foreach (var file in GetCachedFilesNames())
			{
				if (CachDic.ContainsKey(file))
				{
					CachDic[file] = Load(file);
				}
				else
				{
					CachDic.Add(file, Load(file));
				}
			}
			return true;
		}

		DateTime DateToDie
		{
			get
			{
				return DateTime.Now.AddHours(HoursToDie);
			}
		}

		/// <summary>
		/// А можешь ли ты заставить кэш обновиться?
		/// </summary>
		public void HardUpdate()
		{
			Timer.Stop();
			UpdateCache(null, null);
			Timer.Start();			
		}

		public Dictionary<string, DateTime> GetCacheTime(IEnumerable<string> files)
		{
			//var files = GetCachedFilesNames();
			var res = new Dictionary<string,DateTime>();

			foreach (var file in files)
			{
				res.Add(file, (new FileInfo(mainPath + file).LastWriteTime));
			}

			return res;
		}


		private void UpdateCache(object sender, ElapsedEventArgs e)
		{
			UpdateData?.Invoke(CachDic);
		}

		void DataLoad()
		{

			DataLoaded?.Invoke(CachDic);
		}

		/// <summary>
		/// Собственно все данные хранятся тут
		/// string - название класса, кэш которого надо сохранить
		/// </summary>
		Dictionary<string, object> CachDic = new Dictionary<string, object>();
		/// <summary>
		/// Путь хранения кэша
		/// </summary>
		private string mainPath = @"..\Cache.agi\";
		Timer Timer = new Timer();
		public int HoursToDie = 1;

		/// <summary>
		/// Возвращает кэш
		/// </summary>
		/// <param name="category"></param>
		/// <returns></returns>
		public object GetCache(string category)
		{
			if (!CachDic.ContainsKey(category)) CachDic.Add(category, new List<VkDataBase>());
			return CachDic[category];
		}

		/// <summary>
		/// Сохраняет объекты в файл. НУЖНО ТОЛЬКО НАЗВАНИЕ ФАЙЛА
		/// </summary>
		/// <param name="file"></param>
		/// <example>Testcache</example>
		/// <param name="obj"></param>        
		/// <returns></returns>
		public bool Save(string file, IEnumerable obj)
		{
			try
			{
				var formater = new BinaryFormatter();
				using (var write = File.Create(mainPath + file))
				{
					//List<KeyValue<string, object>> dic = null;
					foreach (var el in obj)
					{
						if (el is IDictionary)
							formater.Serialize(write, TransformFromDic(el as IDictionary<string, object>));
						else
							formater.Serialize(write, el);
					}
				}
				return true;
			}
			catch (Exception e)
			{
				return false;
			}

		}

		/// <summary>
		/// Десерелизует дикшенари
		/// </summary>
		/// <param name="dictionary"></param>
		/// <returns></returns>
		private List<KeyValue<string, object>> TransformFromDic(IDictionary<string, object> dictionary)
		{
			var res = new List<KeyValue<string, object>>();
			foreach (var el in dictionary)
			{
				res.Add(new KeyValue<string, object>() { Key = el.Key, Value = el.Value });
			}
			return res;
		}

		/// <summary>
		/// Загружает файл из кэша. НУЖНО ТОЛЬКО ИМЯ
		/// </summary>
		/// <param name="file"></param>
		/// <example>Testcache</example>
		/// <returns></returns>
		public IEnumerable<object> Load(string file)
		{
			try
			{
				var formater = new BinaryFormatter();
				var res = new List<object>();
				using (var read = File.OpenRead(mainPath + file))
				{
					while (read.Position != read.Length)
					{
						var el = formater.Deserialize(read);
						if (el is KeyValue<string, object>)
							res.Add(TransformToDic(formater, read, el as KeyValue<string, object>));
						if (el is IList)
							res.Add(TransformToDic(el as IList));
						else
							res.Add(el);
					}
				}

				return res;
			}
			catch (Exception e)
			{
				return new List<object>();
			}
		}

		/// <summary>
		/// Серелизует в дикшенари
		/// </summary>
		/// <param name="list"></param>
		/// <returns></returns>
		private Dictionary<string, object> TransformToDic(IList list)
		{
			var dic = new Dictionary<string, object>();
			foreach (var el in list as List<KeyValue<string, object>>)
			{
				dic.Add(el.Key, el.Value);
			}
			return dic;
		}

		/// <summary>
		/// Серелизует в дикшенари
		/// </summary>
		/// <param name="formater"></param>
		/// <param name="read"></param>
		/// <param name="curObj"></param>
		/// <returns></returns>
		private Dictionary<string, object> TransformToDic(BinaryFormatter formater,
			FileStream read,
			KeyValue<string, object> curObj)
		{
			var res = new Dictionary<string, object>() { { curObj.Key, curObj.Value } };
			long curPos = read.Position;
			curObj = formater.Deserialize(read) as KeyValue<string, object>;
			while (curObj != null)
			{
				res.Add(curObj.Key, curObj.Value);
				curPos = read.Position;
				curObj = formater.Deserialize(read) as KeyValue<string, object>;
			}

			read.Position = curPos;
			return res;
		}

		/// <summary>
		/// Возвращает имена всех файлов, лежащих в кэше
		/// </summary>
		/// <returns></returns>
		public IEnumerable<string> GetCachedFilesNames()
		{
			if (!Directory.Exists(mainPath)) return new string[0];
			return Directory.GetFiles(mainPath).Select((v) => Path.GetFileNameWithoutExtension(v)).ToList();
		}

	}
}
