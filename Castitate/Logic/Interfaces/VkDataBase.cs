﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Castitate.Logic.Interfaces
{

	public enum VDBStatus { Active, Dead }

	public enum VDBState { Passed, Soon, Future }

	/// <summary>
	/// Класс для получения ключевых даннх
	/// </summary>
	[Serializable]
	public abstract class VkDataBase
	{
		/// <summary>
		/// Получить имя человека, название группы или события
		/// </summary>
		/// <returns></returns>
		public string Name;
		/// <summary>
		/// Важная дата: для события - дата события, для группы - дата последней публикации, для человека - дата последнего сообщения или поста
		/// </summary>
		/// <returns></returns>
		public DateTime Date;
		/// <summary>
		/// Картинка группы, события или ава человека
		/// </summary>
		/// <returns></returns>
		public string ImagePath;
		/// <summary>
		/// Дикшенари может иметь следующие стандартные поля:
		/// MembersCount - int
		/// Passed - bool
		/// Можно писать лубую инфу, но дописывать по типу: Имя класса - Ключ - Значение
		/// Пример: GroupManager - BadGroups - List<string>
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, object> Details;
		/// <summary>
		/// ID, по которому можно работать непосредственно с объектом
		/// </summary>
		public long ID;
		public VDBState State;

		/// <summary>
		/// Сравнивает дату и возвращает её состояние
		/// </summary>
		/// <param name="compare"></param>
		/// <returns></returns>
		public virtual VDBState DateComparer(DateTime compare)
		{
			var res = Date.CompareTo(compare);

			if (res < 0)
			{
				if (Math.Abs((Date - compare).TotalDays) > VkManager.Instance.CastSettings.Data.DaysToDie)
					return VDBState.Passed;
				else
					return VDBState.Soon;
			}
			else if (res == 0)
			{
				return VDBState.Soon;
			}
			else
			{
				return VDBState.Future;
			}
		}

	}
}
