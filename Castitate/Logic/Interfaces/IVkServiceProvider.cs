﻿using Castitate.GUI.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Castitate.Logic.Interfaces
{
	/// <summary>
	/// Интерфейс связка между фронтом и бэком.
	/// Служит для получения различных данных и манипуляции с ними
	/// </summary>
	public interface IVkServiceProvider  
	{
        /// <summary>
        /// Ивент срабатывает при обновлении данных из кэша
        /// </summary>
        event Action<IEnumerable<VkDataBase>> DataUpdated;

		/// <summary>
		/// Получить все данные: все группы или события или друзей
		/// </summary>
		/// <returns></returns>
		IEnumerable<VkDataBase> GetAllData();
		/// <summary>
		/// Получить данные, у которые MainDate совпадает с ti
		/// </summary>
		/// <param name="ti"></param>
		/// <returns></returns>
		IEnumerable<VkDataBase> GetAllDataAtTime(DateTime ti);
		/// <summary>
		/// Получить данные, у которых MainDate до ti
		/// </summary>
		/// <param name="ti"></param>
		/// <returns></returns>
		IEnumerable<VkDataBase> GetAllDataBeforeTime(DateTime ti);
		/// <summary>
		/// Получить данные, у которых MainDate после ti
		/// </summary>
		/// <param name="ti"></param>
		/// <returns></returns>
		IEnumerable<VkDataBase> GetAllDataAfterTime(DateTime ti);
		/// <summary>
		/// Удаляет определённые данные, если успешно возвращает true
		/// </summary>
		/// <param name="datas"></param>
		/// <returns></returns>
		Task<bool> Removed(IEnumerable<VkDataBase> datas);

		/// <summary>
		/// Возвращает устаревшую дату (любая дата с State.Passed)
		/// </summary>
		/// <returns></returns>
		IEnumerable<VkDataBase> GetOldData();

		/// <summary>
		/// False - done all job
		/// </summary>
		/// <returns></returns>
		bool IsBusy();
	
	}
}
