﻿using Castitate.GUI.Model;
using Castitate.Logic.DataClasses;
using Castitate.Logic.Helpers;
using Castitate.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using VkNet.Categories;
using VkNet.Enums.Filters;
using VkNet.Model.RequestParams;
using System.Collections;
using System.Threading.Tasks;

namespace Castitate.Logic.Entity
{
	public class GroupServiceProvider : IVkServiceProvider
	{
		private GroupsCategory groupsApi;
		private long userId;
		private GroupsGetParams template;
		bool status = false;

		public GroupServiceProvider(GroupsCategory groups, long usrID)
		{
			groupsApi = groups;
			userId = usrID;
			template = new GroupsGetParams()
			{
				UserId = userId,
				Filter = GroupsFilters.Groups,
				Extended = true,
				Fields = GroupsFields.All
			};

			VkCachManager.Instance.UpdateData += VkCachManager_UpdateData;
			VkCachManager.Instance.DataLoaded += (v) =>
			{
				if (v.Keys.Contains(nameof(GroupServiceProvider)))
					DataUpdated?.Invoke(v[nameof(GroupServiceProvider)] as IEnumerable<VkDataBase>);
			};
		}

		private async void VkCachManager_UpdateData(Dictionary<string, object> obj)
		{
			status = true;
			await TaskManager.RunTask(() =>
			{
				obj[nameof(GroupServiceProvider)] = SetCache();
				VkCachManager.Instance.Save(nameof(GroupServiceProvider), obj[nameof(GroupServiceProvider)] as IEnumerable<VkDataBase>);

			});

			DataUpdated?.Invoke(obj[nameof(GroupServiceProvider)] as IEnumerable<VkDataBase>);
			status = false;
		}

		public event Action<IEnumerable<VkDataBase>> DataUpdated;

		public IEnumerable<VkDataBase> GetAllData()
		{
			return (VkCachManager.Instance.GetCache(nameof(GroupServiceProvider)) as IEnumerable).Cast<VkDataBase>();
		}

		public IEnumerable<VkDataBase> GetAllDataAfterTime(DateTime ti)
		{
			return (VkCachManager.Instance.GetCache(nameof(GroupServiceProvider)) as IEnumerable).Cast<VkDataBase>().Where((v) => v.DateComparer(ti) == VDBState.Future);
		}

		public IEnumerable<VkDataBase> GetAllDataAtTime(DateTime ti)
		{
			return (VkCachManager.Instance.GetCache(nameof(GroupServiceProvider)) as IEnumerable).Cast<VkDataBase>().Where((v) => v.DateComparer(ti) == VDBState.Soon);
		}

		public IEnumerable<VkDataBase> GetAllDataBeforeTime(DateTime ti)
		{
			return (VkCachManager.Instance.GetCache(nameof(GroupServiceProvider)) as IEnumerable).Cast<VkDataBase>().Where((v) => v.DateComparer(ti) == VDBState.Passed);
		}

		public IEnumerable<VkDataBase> GetOldData()
		{
			return (VkCachManager.Instance.GetCache(nameof(GroupServiceProvider)) as IEnumerable).Cast<VkDataBase>().Where((v) => v.DateComparer(DateTime.Now) == VDBState.Passed);
		}

		public async Task<bool> Removed(IEnumerable<VkDataBase> groups)
		{
			return await Task.Run(() =>
			{
				try
				{
					var cache = VkCachManager.Instance.GetCache(nameof(GroupServiceProvider)) as List<VkDataBase>;

					foreach (var group in groups.Cast<VkDataBase>())
					{
						groupsApi.Leave(group.ID);
						cache.Remove(group);
					}
					return true;
				}
				catch
				{
					return false;
				}
			});
		}

		/// <summary>
		/// Обязательно нужна копия геттера, иначе теплейт собьётся, 
		/// так как используется оффсет (сдвиг)
		/// </summary>
		/// <param name="getter"></param>
		/// <returns></returns>
		IEnumerable<VkDataBase> ExtractData(GroupsGetParams getter)
		{
			var res = new List<VkDataBase>();

			do
			{
				int count = 0;
				foreach (var group in groupsApi.Get(getter))
				{
					if (group == null) break;
					res.Add(new VkGroupData(group));
					count++;
					// if (count % 5 == 0) Thread.Sleep(3500);
				}

				getter.Offset += count;



			} while (false);

			return res;
		}

		IEnumerable SetCache()
		{
			return ExtractData(template.Copy());
		}

		public bool IsBusy()
		{
			return status;
		}
	}
}
