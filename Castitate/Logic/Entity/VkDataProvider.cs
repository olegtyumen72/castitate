﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VkNet.Categories;
using VkNet.Enums.SafetyEnums;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace Castitate.Logic.Entity
{
    public class VkDataProvider
    {

        WallCategory postsApi;
        DocsCategory docApi;
        AudioCategory audioApi;
        MessagesCategory mssgApi;

        public VkDataProvider(WallCategory apiW, DocsCategory apiD, AudioCategory apiA, MessagesCategory apiM)
        {
            postsApi = apiW;
            docApi = apiD;
            audioApi = apiA;
            mssgApi = apiM;
        }

        /// <summary>
        /// Возвращает ближаюшую дату к опорной
        /// </summary>
        /// <param name="Id">группы</param>
        /// <param name="comparer">опорная дата</param>
        /// <returns>Ближайшая дата к опорной</returns>
        public DateTime GetLastActiveDataGroup(long Id, Group elem, DateTime comparer)
        {
            var lastPost = DateTime.Now.AddYears(-2000);
            var lastDoc = DateTime.Now.AddYears(-2000);
            var lastMusic = DateTime.Now.AddYears(-2000);

			var tmp = postsApi.Get(new WallGetParams()
			{
				Count = 3,
				Offset = 0,
				OwnerId = -Id,
			});


			lastPost = tmp.WallPosts.Where((v) => v.IsPinned == null || v.IsPinned == false).First().Date.Value;

			

            if (elem.CanUploadDocuments)
                lastDoc = docApi.Get(1, 0, -Id).First().Date.Value;

            lastMusic = audioApi.Get(-Id, null, null, 0, 0).First().Date.Value;


            return ClosestData((new List<DateTime>() { lastPost, lastDoc, lastMusic }), comparer);

        }

       DateTime ClosestData(List<DateTime> dates, DateTime comparer)
        {
            var distances = dates
                .Select(p => Math.Abs((comparer - p).Ticks)).ToList();

            var distance = long.MaxValue;

            foreach (var pair in distances)
            {
                if (pair < distance)
                {
                    distance = pair;
                }
            }

            return dates[distances.IndexOf(distance)];
        }


        /// <summary>
        /// Возвращает дату ближаюшую от опорной
        /// </summary>
        /// <param name="Id">id человека</param>
        /// <param name="elem">пользователь</param>
        /// <param name="comparer">опорная дата</param>
        /// <returns>возвращает ближайшую дату</returns>
        public DateTime GetLastActiveDataFriend(long Id, User elem, DateTime comparer)
        {
            var lastPost = DateTime.Now.AddYears(-2000);
            var lastDoc = DateTime.Now.AddYears(-2000);
            var lastMusic = DateTime.Now.AddYears(-2000);
            var lastMess = DateTime.Now.AddYears(-2000);

            if (elem.CanPost)
            {

                var res = postsApi.Get(new WallGetParams()
                {
                    Count = 1,
                    Offset = 0,
                    OwnerId = Id
                }).WallPosts;

                if (res.Count > 0)
                {
                    lastPost = res.First().Date.Value;
                }
            }


            if (elem.CanWritePrivateMessage)
                lastMess = mssgApi.Get(new MessagesGetParams()
                {
                    Count = 1,
                    Offset = 0,
                    TimeOffset = 0,
                    Out = VkNet.Enums.MessageType.Received,
                    Filters = VkNet.Enums.MessagesFilter.All,
                    PreviewLength = 1
                }).Messages.First().Date.Value;

            //if (elem.CanSeeAudio)
            //    lastMusic = audioApi.Get(Id, null, null, 0, 0).First().Date.Value;


            return ClosestData((new List<DateTime>() { lastPost, lastDoc, lastMusic, lastMess }), comparer);
        }

    }
}
