﻿using Castitate.Logic.DataClasses;
using Castitate.Logic.Helpers;
using Castitate.Logic.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet.Categories;
using VkNet.Enums.Filters;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace Castitate.Logic.Entity
{
	/// <summary>
	/// Описание смотри в Interfaces/IVkServiceProvider.cs
	/// </summary>
	class EventServiceProvider : IVkServiceProvider
	{
		private GroupsCategory eventApi;
		private long userId;
		private GroupsGetParams template;
		private bool status = false;

		public event Action<IEnumerable<VkDataBase>> DataUpdated;

		static EventServiceProvider()
		{

		}

		public EventServiceProvider(GroupsCategory groups, long userId)
		{
			eventApi = groups;
			this.userId = userId;
			template = new GroupsGetParams()
			{
				UserId = userId,
				Filter = GroupsFilters.Events,
				Extended = true,
				Fields = GroupsFields.All
			};
			
			VkCachManager.Instance.UpdateData += VkCachManager_UpdateData;
			VkCachManager.Instance.DataLoaded += (v) =>
			{
				if (v.Keys.Contains(nameof(EventServiceProvider)))
				{
					DataUpdated?.Invoke(v[nameof(EventServiceProvider)] as IEnumerable<VkDataBase>);
				}
			};
		}

		private async void VkCachManager_UpdateData(Dictionary<string, object> obj)
		{
			status = true;
			await TaskManager.RunTask(() =>
			{
				//ProgressReporter.SetProgress(30);
				//ProgressReporter.SetText("Обнов. события");

				obj[nameof(EventServiceProvider)] = SetCache();

				//ProgressReporter.SetProgress(70);
				//ProgressReporter.SetText("Сохр. события");

				VkCachManager.Instance.Save(nameof(EventServiceProvider), obj[nameof(EventServiceProvider)] as IEnumerable<VkDataBase>);

				//ProgressReporter.SetProgress(100);
				//ProgressReporter.SetText("Готово");
			});
			DataUpdated?.Invoke(obj[nameof(EventServiceProvider)] as IEnumerable<VkDataBase>);
			status = false;
		}

		public IEnumerable<VkDataBase> GetAllData()
		{
			return (VkCachManager.Instance.GetCache(nameof(EventServiceProvider)) as IEnumerable).Cast<VkDataBase>();
		}

		public IEnumerable<VkDataBase> GetAllDataAfterTime(DateTime ti)
		{
			return (VkCachManager.Instance.GetCache(nameof(EventServiceProvider)) as IEnumerable).Cast<VkDataBase>().Where((v) => v.DateComparer(ti) == VDBState.Future);
		}

		public IEnumerable<VkDataBase> GetAllDataAtTime(DateTime ti)
		{
			return (VkCachManager.Instance.GetCache(nameof(EventServiceProvider)) as IEnumerable).Cast<VkDataBase>().Where((v) => v.DateComparer(ti) == VDBState.Soon);
		}

		public IEnumerable<VkDataBase> GetAllDataBeforeTime(DateTime ti)
		{
			return (VkCachManager.Instance.GetCache(nameof(EventServiceProvider)) as IEnumerable).Cast<VkDataBase>().Where((v) => v.DateComparer(ti) == VDBState.Passed);
		}

		public IEnumerable<VkDataBase> GetOldData()
		{
			return (VkCachManager.Instance.GetCache(nameof(EventServiceProvider)) as IEnumerable).Cast<VkDataBase>().Where((v) => v.DateComparer(DateTime.Now) == VDBState.Passed);
		}

		public async Task<bool> Removed(IEnumerable<VkDataBase> events)
		{
			return await Task.Run(() =>
		   {
			   try
			   {
				   var cache = VkCachManager.Instance.GetCache(nameof(EventServiceProvider)) as List<VkDataBase>;

				   foreach (var evnt in events)
				   {
					   eventApi.Leave(evnt.ID);
					   cache.Remove(evnt);
				   }
				   return true;
			   }
			   catch
			   {
				   return false;
			   }
		   });
		}

		/// <summary>
		/// Обязательно нужна копия геттера, иначе теплейт собьётся, 
		/// так как используется оффсет (сдвиг)
		/// </summary>
		/// <param name="getter"></param>
		/// <returns></returns>
		IEnumerable<VkDataBase> ExtractData(GroupsGetParams getter)
		{
			var res = new List<VkDataBase>();

			do
			{
				int count = 0;
				foreach (var group in eventApi.Get(getter))
				{
					if (group == null) break;
					res.Add(new VkEventData(group));
					count++;
				}
				getter.Offset += count;

			} while (false);

			return res;
		}

		IEnumerable<VkDataBase> SetCache()
		{
			return ExtractData(template.Copy());
		}

		public bool IsBusy()
		{
			return status;
		}
	}
}
