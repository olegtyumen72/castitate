﻿using Castitate.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VkNet.Categories;
using VkNet.Model;
using VkNet.Model.RequestParams;
using VkNet.Enums;
using VkNet.Enums.Filters;
using VkNet.Enums.SafetyEnums;
using Castitate.Logic.DataClasses;
using Castitate.Logic.Helpers;
using System.Collections;
using System.Threading.Tasks;

namespace Castitate.Logic.Entity
{
	public class FriendServiceProvider : IVkServiceProvider
	{
		private FriendsCategory friendsApi;
		private long userID;
		FriendsGetParams template;
		bool status = false;

		public FriendServiceProvider(FriendsCategory friends, long usrID)
		{
			friendsApi = friends;
			userID = usrID;
			template = new FriendsGetParams()
			{
				UserId = userID,
				Fields = ProfileFields.All,
				NameCase = NameCase.Nom,
				Order = FriendsOrder.Hints
			};

			VkCachManager.Instance.UpdateData += VkCachManager_UpdateData;
			VkCachManager.Instance.DataLoaded += (v) =>
			{
				if (v.Keys.Contains(nameof(FriendServiceProvider)))
					DataUpdated?.Invoke(v[nameof(FriendServiceProvider)] as IEnumerable<VkDataBase>);
			};
		}

		public event Action<IEnumerable<VkDataBase>> DataUpdated;

		private async void VkCachManager_UpdateData(Dictionary<string, object> obj)
		{
			status = true;
			await TaskManager.RunTask(() =>
			{
				obj[nameof(FriendServiceProvider)] = SetCache();
				VkCachManager.Instance.Save(nameof(FriendServiceProvider), obj[nameof(FriendServiceProvider)] as IEnumerable<VkDataBase>);
			});

			DataUpdated?.Invoke(obj[nameof(FriendServiceProvider)] as IEnumerable<VkDataBase>);
			status = false;
		}

		/// <summary>
		/// Обязательно нужна копия геттера, иначе теплейт собьётся, 
		/// так как используется оффсет (сдвиг)
		/// </summary>
		/// <param name="getter"></param>
		/// <returns></returns>
		IEnumerable ExtractData(FriendsGetParams getter)
		{
			var res = new List<VkDataBase>();

			do
			{
				int count = 0;
				foreach (var friend in friendsApi.Get(getter))
				{
					if (friend == null) break;
					res.Add(new VkFriendData(friend));
					count++;
				}
				getter.Offset += count;

			} while (false);

			return res;
		}

		IEnumerable SetCache()
		{
			return ExtractData(template.Copy());
		}


		public IEnumerable<VkDataBase> GetAllData()
		{
			return (VkCachManager.Instance.GetCache(nameof(FriendServiceProvider)) as IEnumerable).Cast<VkDataBase>();
		}

		public IEnumerable<VkDataBase> GetAllDataAfterTime(DateTime ti)
		{
			return (VkCachManager.Instance.GetCache(nameof(FriendServiceProvider)) as IEnumerable).Cast<VkDataBase>().Where((v) => v.DateComparer(ti) == VDBState.Future);
		}

		public IEnumerable<VkDataBase> GetAllDataAtTime(DateTime ti)
		{
			return (VkCachManager.Instance.GetCache(nameof(FriendServiceProvider)) as IEnumerable).Cast<VkDataBase>().Where((v) => v.DateComparer(ti) == VDBState.Soon);
		}

		public IEnumerable<VkDataBase> GetAllDataBeforeTime(DateTime ti)
		{
			return (VkCachManager.Instance.GetCache(nameof(FriendServiceProvider)) as IEnumerable).Cast<VkDataBase>().Where((v) => v.DateComparer(ti) == VDBState.Passed);
		}

		public IEnumerable<VkDataBase> GetOldData()
		{
			return (VkCachManager.Instance.GetCache(nameof(FriendServiceProvider)) as IEnumerable).Cast<VkDataBase>().Where((v) => v.DateComparer(DateTime.Now) == VDBState.Passed);
		}

		public async Task<bool> Removed(IEnumerable<VkDataBase> users)
		{
			return await Task.Run(() =>
			{
				try
				{
					var cache = VkCachManager.Instance.GetCache(nameof(FriendServiceProvider)) as List<VkDataBase>;

					foreach (var usr in users)
					{
						friendsApi.Delete(usr.ID);
						cache.Remove(usr);
					}
					return true;
				}
				catch
				{
					return false;
				}
			});
		}

		public bool IsBusy()
		{
			return status;
		}
	}
}
