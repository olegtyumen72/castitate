﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Castitate.Logic
{
    /// <summary>
    /// ОЧЕНЬ СЛОЖНА! Должен был быть класс для хранения в очереди задач и запуска в промежутке каждой из них
    /// </summary>
    public class TaskManager
    {       

        /// <summary>
        /// А сможешь ли ты осилить ещё одну задачку, а?
        /// </summary>
        /// <param name="act"></param>
        public static async Task RunTask(Action act)
        {

            await Task.Factory.StartNew(act);

        }      

    }
}
